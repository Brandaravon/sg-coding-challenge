﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
//This code is not unity-specific except for Random.Range used for adding test data.
using UnityEngine;

public class PopulationCalculator {

    //I did a brief bit of research on Unity XML serialization to create test data more easily
    //Then I created this class
    [XmlRoot("population")]
    public class PopulationContainer
    {
        [XmlArrayItem("person")]
        public List<Person> Persons = new List<Person>();

        public void Save(string path)
        {
            var serializer = new XmlSerializer(typeof(PopulationContainer));
            using (var stream = new FileStream(path, FileMode.Create))
            {
                serializer.Serialize(stream, this);
            }
        }
    }

    //My initial class for population data storage
    public class Person
    {
        protected int m_birthYear;
        [XmlAttribute("birth")]
        public int BirthYear
        {
            set { m_birthYear = value; }
            get { return m_birthYear; }
        }

        protected int m_deathYear;
        [XmlAttribute("death")]
        public int DeathYear
        {
            set { m_deathYear = value; }
            get { return m_deathYear; }
        }
    }

    //These are also accesed by the graph
    protected static int MIN_YEAR = 1900;
    public static int MinYear
    {
        get { return MIN_YEAR; }
    }
    protected static int MAX_YEAR = 2000;

    //Does a person count as being alive in the year where they die?
    //Rather than ask the question via e-mail, I added a toggle for it.
    protected bool m_deathYearInclusive = true;
    public bool DeathYearInclusive
    {
        set { m_deathYearInclusive = value; }
        get { return m_deathYearInclusive; }
    }

    //List of people to perform calculations on
    protected List<Person> m_population = new List<Person>();

    //Graph of populations over the years, accessible by rendering functionality
    protected int[] m_populationGraph = null;
    public int[] PopulationGraph
    {
        get { return m_populationGraph; }
    }

    //Storage of last calculation of max year, to allow it to be highlighted on the graph
    protected int m_latestMaxYear = 0;
    public int LatestMaxYear
    {
        get { return m_latestMaxYear; }
    }

    //Basic xml loading, was written prior to seralization research, so this could be
    //more elegant and automated.
    public void LoadPopulation(string filename)
    {
        XmlDocument populationDocument = new XmlDocument();
        populationDocument.Load(filename);
        XmlElement rootElement = populationDocument.DocumentElement;
        XmlNode personsList = rootElement.FirstChild;
        foreach (XmlNode node in personsList.ChildNodes)
        {
            Person newPerson = new Person();
            newPerson.BirthYear = int.Parse(node.Attributes["birth"].Value);
            newPerson.DeathYear = int.Parse(node.Attributes["death"].Value);
            m_population.Add(newPerson);
        }        
    }

    //Test data generation function, created to speed up testing
    public void GenerateTestData(string filename, int testDataCount)
    {
        PopulationContainer popCon = new PopulationContainer();
        for (int i =0; i<testDataCount; i++)
        {
            Person testPerson = new Person();
            int random1 = MIN_YEAR + Random.Range(0, 100);
            int random2 = MIN_YEAR + Random.Range(0, 100);
            if (random1 > random2) {
                testPerson.BirthYear = random2;
                testPerson.DeathYear = random1;
            }
            else 
            {
                testPerson.BirthYear = random1;
                testPerson.DeathYear = random2;
            }
            popCon.Persons.Add(testPerson);
        }

        popCon.Save(filename);        
    }

    //Here's the important part, the actual calculations
    public int FindHighestPopulationYear()
    {
        int yearRange = MAX_YEAR - MIN_YEAR;
        
        //We don' actually need the list of people, just a list of how much the population changes
        //each year, so I create data to store that per year.
        int[] populationChanges = new int[yearRange];
        m_populationGraph = new int[yearRange];

        foreach (Person person in m_population)
        {            
            int index = person.BirthYear - MIN_YEAR;
            populationChanges[index] += 1;
            if (m_deathYearInclusive) {
                index = (person.DeathYear + 1) - MIN_YEAR;
                //If a person is considered alive in the year they die and they die on the last year
                //their death is not actually factored in, and would overrun the array if it was!
                if (index < populationChanges.Length)
                {
                    populationChanges[index] -= 1;
                }
            }
            else
            {
                index = person.DeathYear - MIN_YEAR;
                populationChanges[index] -= 1;
            }        
        }
        int maxPopIndex = 0;
        int maxPopulation = 0;
        int currentPopulation = 0;
        //Now we just iterate through the years and calculate the population at each year, both
        //to find the max and to create a graph of population over time.
        for (int i = 0; i < populationChanges.Length; i++)
        {
            currentPopulation += populationChanges[i];
            m_populationGraph[i] = currentPopulation;
            if (currentPopulation > maxPopulation)
            {
                maxPopulation = currentPopulation;
                maxPopIndex = i;
            }
        }

        m_latestMaxYear = maxPopIndex + MIN_YEAR;
        return maxPopIndex + MIN_YEAR;
    }
	
}
