﻿using UnityEngine;
using System.Collections.Generic;

public class PopulationGraph : MonoBehaviour {
    //A Unity-based test class for rendering a simple bar graph.
       
    protected PopulationCalculator m_calculator = new PopulationCalculator();
    [SerializeField]
    protected Transform m_populationGraphRoot = null;
    //Location of graph in 3D Space
    [SerializeField]
    protected GameObject m_populationGraphPrefab = null;
    [SerializeField]
    protected GameObject m_populationMaxGraphPrefab = null;
    //Prefab bars, these could be any asset that can be scaled and translated.
    [SerializeField]
    protected float m_graphObjectOffset = 1.0f;
    [SerializeField]
    protected float m_graphScaleModifier = 1.0f;
    //Used to determine the size of the graph
    //In the future, the graph could dynamically scale itself based on the max value

    protected List<GameObject> m_graphBars = new List<GameObject>();
    //Hold the list of bars in case they need to be destroyed or altered

    // Use this for initialization
    void Start () {
        //RequestTestData();
        m_calculator.LoadPopulation("Assets/Data/population4.xml");
        Debug.Log(m_calculator.FindHighestPopulationYear().ToString());
        //This performs the core purpose of the exercize and writes the results to the console
        DrawPopulationGraph(m_calculator.PopulationGraph);
        //This draws the graph in the Unity program
    }	

    public void RequestTestData()
    {
        //This is where I generated the four test xml files 
        m_calculator.GenerateTestData("Assets/Data/population1.xml", 100);
        m_calculator.GenerateTestData("Assets/Data/population2.xml", 250);
        m_calculator.GenerateTestData("Assets/Data/population3.xml", 500);
        m_calculator.GenerateTestData("Assets/Data/population4.xml", 1000);
    }

    public void DrawPopulationGraph (int[] populationData)
    {
        ClearGraph();
        //Clear previous graph if there is one
        if (m_populationMaxGraphPrefab==null || m_populationGraphRoot == null || m_populationGraphPrefab == null || populationData == null)
        {
            Debug.LogError("Missing data or assets for graph rendering.");
            return;
        }
        for (int i=0; i<populationData.Length; i++)
        {
            int yearIndex = i + PopulationCalculator.MinYear;
            GameObject barprefab = null;
            if (m_calculator.LatestMaxYear == yearIndex)
            {
                barprefab = m_populationMaxGraphPrefab;
            }
            else
            {
                barprefab = m_populationGraphPrefab;
            }
            //Draw a normal or 'special' bar if it is the max population year.
            GameObject graphBar = (GameObject)GameObject.Instantiate(barprefab, m_populationGraphRoot.position, m_populationGraphRoot.rotation);
            graphBar.name = "Population " + yearIndex.ToString();
            graphBar.transform.parent = m_populationGraphRoot.transform;
            graphBar.transform.Translate(new Vector3(m_graphObjectOffset * i, m_graphScaleModifier * populationData[i]/2.0f, 0.0f));
            graphBar.transform.localScale = new Vector3(1.0f, m_graphScaleModifier * populationData[i], 1.0f);
            m_graphBars.Add(graphBar);
            //Basic instantiation, positioning, and scaling of a box to create a bar graph.
        }
    }

    public void ClearGraph()
    {
        foreach (GameObject gameObject in m_graphBars)
        {
            Destroy(gameObject);
        }
        m_graphBars.Clear();
    }

}
